package com.example.task03_room;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task03_room.db.AppDatabase;
import com.example.task03_room.db.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText username,password,repassword,phone;
    Button login,reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.pwd);
        repassword = (EditText) findViewById(R.id.rePwd);
        phone= (EditText) findViewById(R.id.phnNo);
        login = (Button) findViewById(R.id.btnLogIn);
        reg = (Button) findViewById(R.id.btnRegister);


        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user = username.getText().toString();
                String pass = password.getText().toString();
                String repass = repassword.getText().toString();
                String phoneNo = phone.getText().toString();

                //Check whether all fields are filled
                if(user.equals("")||pass.equals("")||repass.equals("")||phoneNo.equals("")){
                    Toast.makeText(MainActivity.this, "Please enter all the fields", Toast.LENGTH_SHORT).show();
                //Check whether password is strong enough
                }else if( password.getText().length()<=7){
                    Toast.makeText(MainActivity.this, "Password is too weak! Enter at least 8 characters", Toast.LENGTH_SHORT).show();
                //Check whether Phone No is a 10 digits number
                }else if( phone.getText().length()!=10){
                    Toast.makeText(MainActivity.this, "Enter only 10 digits for Phone No", Toast.LENGTH_SHORT).show();
                }
                else {
                    //Check whether password equals to re-entered password
                    if(pass.equals(repass)){
                        //Check whether user already exits in Database
                        if (checkUser(user)== true) {
                            saveNewUser(user, pass, phoneNo);
                            Toast.makeText(MainActivity.this, "Registration Successful!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LogIn.class);
                            startActivity(intent);
                        }else {
                            Toast.makeText(MainActivity.this, "User Already exists!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(MainActivity.this, "Not Matching Passwords", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LogIn.class);
                startActivity(intent);
            }
        });
    }

    //Insert user details to the Database
    private void saveNewUser(String username, String password, String phoneNo){
        AppDatabase db = AppDatabase.getDbInstance(this.getApplicationContext());

        User user = new User();
        user.userName = username;
        user.password = password;
        user.phoneNo = phoneNo;
        db.userDao().insertUser(user);

        finish();
    }

    //Check whether username already exits in Database
    private Boolean checkUser(String userName){
        AppDatabase db  = AppDatabase.getDbInstance(this.getApplicationContext());

        User user = new User();
        user.userName = username.getText().toString();
        if (db.userDao().getUser(username.getText().toString())!= null){
            return false;
        }else {
            return true;
        }
    }
}