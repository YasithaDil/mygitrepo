package com.example.task03_room;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task03_room.db.AppDatabase;
import com.example.task03_room.db.User;

import java.util.List;

public class LogIn extends AppCompatActivity {

    EditText username, password;
    Button btnLOGIN, btnRegPg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        username = (EditText) findViewById(R.id.userNm);
        password = (EditText) findViewById(R.id.userpwd);
        btnLOGIN = (Button) findViewById(R.id.btnLOGIN);

        btnLOGIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user = username.getText().toString();
                String pass = password.getText().toString();

                //Check whether all fields are filled
                if(user.equals("") || pass.equals(""))
                    Toast.makeText(LogIn.this, "Please enter all the fields", Toast.LENGTH_SHORT).show();
                else{
                    //Check whether entered username & password are correct
                    if (checkUsernamePassword(user,pass) == true){
                         Toast.makeText(LogIn.this, "LogIn Successful!", Toast.LENGTH_SHORT).show();
                         Intent intent = new Intent(getApplicationContext(),Home.class);
                         startActivity(intent);
                    }else {
                        Toast.makeText(LogIn.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        btnRegPg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });
    }

    //Username & Password Authentication
    public Boolean checkUsernamePassword(String username, String password) {
        AppDatabase db = AppDatabase.getDbInstance(this.getApplicationContext());
        User user = new User();
        user.userName = username;
        user.password = password;

        if (db.userDao().getUserPass(username.toString(), password.toString()) != null) {
            return true;
        } else {
            return false;
        }
    }

}